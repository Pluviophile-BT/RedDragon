//response code
//100
#define RD_CONTINUE              "100 Continue"
#define RD_SWITCHPRO            "101 Switching Protocols"
//200
#define RD_OK                            "200 OK"
#define RD_CREATED                  "201 Created"
#define RD_ACCEPTED                "202 Accepted"
#define RD_NONAUTHINFO       "203 Non-Authoritative Information"
#define RD_NOCONTENT            "204 No Content"
#define RD_RESET                        "205 Reset Content"
#define RD_PARCONTENT           "206 Partial Content"
//300
#define RD_MULTICHOICE          "300 Multiple Choices"
#define RD_MOVPERM                 "301 Moved Permanently"
#define RD_FOUND                      "302 Found"
#define RD_SEEOTHER                "303 See Other"
#define RD_NOTMOD                  "304 Not Modified"
#define RD_USERPROXY              "305 Use Proxy"
#define RD_TEMPREDI                 "307 Temporary Redirect"
//400
#define RD_BADREQ                    "400 Bad Request"
#define RD_UNAUTH                    "401 Unauthorized"
#define RD_FORBIDDEN              "403 Forbidden"
#define RD_NOTFOUND               "404 Not Found"
#define RD_METHODNALLOW     "405 Method Not Allowed"
#define RD_NOTACCEPT               "406 Not Acceptable"
#define RD_PROXYAUTHREQ       "407 Proxy Authentication Required"
#define RD_REQTIMEOUT            "408 Request Time-out"
#define RD_CONFLICT                  "409 Conflict"
#define RD_GONE                         "410 Gone"
#define RD_LENREQ                      "411 Length Required"
#define RD_PREFAILED                 "412 Precondition Failed"
#define RD_REQLARGE                 "413 Request Entity Too Large"
#define RD_URITOOLONG            "414 Request-URI Too Large"
#define RD_UNMEDIATYPE          "415 Unsupported Media Type"
#define RD_REQRANGE                "416 Requested range not satisfiable"
#define RD_EXCEPTFAILED           "417nExpectation Failed"
//500
#define RD_SERVERINERR            "500 Internal Server Error"
#define RD_SERVERIMPLE            "501 Not Implemented"
#define RD_SERVERBADGAT        "502 Bad Gateway"
#define RD_SERVICEUN                "503 Service Unavailable"
#define RD_GATTIMEOUT            "504 Gateway Time-out"
#define RD_HTTPVERERR             "505 HTTP Version not "





