#ifndef RDGMTTIME_H
#define RDGMTTIME_H

#include<iostream>

using namespace std;

class RDGmtTime
{
private:
public:
    RDGmtTime();
    static string getGmtTime();
    ~RDGmtTime();
};
#endif